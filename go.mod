module gitlab.com/doniapr-gopkg/utilities

go 1.18

require golang.org/x/text v0.12.0
require	github.com/davecgh/go-spew v1.1.1
require	github.com/google/uuid v1.3.0
require	github.com/mitchellh/mapstructure v1.3.3
require	github.com/segmentio/encoding v0.2.0
require	github.com/stretchr/testify v1.6.1